require 'rubygems'
require 'bundler/setup'
Bundler.require(:default)

hostname = `hostname`

get '/' do
<<EOS
<HTML>
  <HEAD>
    <TITLE>HELLOWORLD</TITLE>
  </HEAD>
  <BODY>
    <h1>Hello world!(v3) ZZ FROM #{hostname.strip}:#{settings.port}</h1>
  </BODY>
</HTML>
EOS
end
